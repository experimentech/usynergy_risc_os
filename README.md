Port of uSynergy client to RISC OS.

 Synergy https://symless.com/synergy is a product which allows sharing of peripherals such as mouse, keyboard and joystick as well as the clipboard to other devices over the network. The devices behave like a multi monitor setup on a single computer.

 uSynergy is a lightweight library designed for embedded devices.

 uSynergy for RISC OS utilises the uSynergy library to share mouse and keyboard between the host (Only tested with Linux), and the RISC OS client.

 This repository is nothing more than a test program at this point which I am using to work out how to do things. As such it is very crude and many things are wrong. This being said, it is usable.

 How to use it:

 *Clone the repository. I'm currently using sgit on RISC OS for this.

 *in uSynergyRISCOS.c find uSynSrvName, and change the IP address to the IP address of your Synergy server. The Synergy server shows the IP address in its configuration. If you wish, also change the text in context.m_clientName to another name for your RISC OS client. This is the name which has to be set in "Aliases" in the synergy server for the client.

 *Ensure you have !GCC and !UnixLib loaded and ready to go. run "make" from a TaskWindow. Hopefully it will build the library, and a dynamic and static version of the client.

 *Assuming it built, run the client from a TaskWindow. I have been using susynergy (static client). Dynamic hasn't been tested in a long time.

 *Once running it should say the connection is successful along with a whole lot of other info being used for debugging.

 *Drag the mouse on your server over to the direction you configured the client screen and give it a try!

 
 Misc:

 The keyboard was mapped empirically, so they may be errors or omissions. Scroll lock isn't passed on to the client.

 the uSynergy library source is untouched.

 Many things in the client are done poorly / incorrectly. It's not intended to be a releasable product. More of a tech test / stick to poke at things with. I released it so others can have a play. 


 What works:
 *Keyboard (US English).

 *Mouse.

 *What doesn't work / isn't implemented:

 *Clipboard.

 *Dragging scrollbars properly.

 *Scrollwheel scrolls too fast.

 *An elegant means to exit uSynergy 