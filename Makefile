CC = gcc
CFLAGS +=

SOURCES = uSynergy.c uSynergyRISCOS.c
OBJECTS = $(SOURCES:.c=.o)
LIBS := -LOSLib -IOSLib: -LOSLib: -lOSLib32 -luSynergy

.PHONY: clean all
.DEFAULT_GOAL := all
#???

all: dlibusynergy slibusynergy susynergy dusynergy

%.o: %.c
	$(CC) $(CFLAGS) $< -o $@ -c $(LIBS)

dusynergy:
	$(CC) $(CFLAGS) -luSynergy uSynergyRISCOS.c keyboard.c -L. $^ -o $@

susynergy:
	$(CC) $(CFLAGS) -static -luSynergy uSynergyRISCOS.c keyboard.c -L. $^ -o $@


#FIXME
dlibusynergy:
	$(CC) -o o.libuSynergy -c c.uSynergy
	ar rcs libuSynergy.a libuSynergy.o

#The only difference is -static
slibusynergy:
	$(CC) -static -o o.libuSynergy -c c.uSynergy
	ar rcs libuSynergy.a libuSynergy.o


clean:
	rm -rf *.o

clean_ro:
	delete o.libuSyn
	delete susynergy
	